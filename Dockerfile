FROM node:latest

LABEL author="罗嘉政 <<3157340839@qq.com>>"
LABEL version="1.0"
LABEL description="mysql-express"

ENV path=/root/node/mysql-express

RUN cd /root && mkdir -p node/mysql-express
COPY ./mysql-express $path

WORKDIR ${path}

RUN npm install

EXPOSE 3000
ENTRYPOINT ["npm","run","start"]

