import db from './mysql'
/**
 * 
 * @param {string} sql 需要传入的sql语句
 * @param {Array} args 需要传入的sql参数
 * @returns {Promise}  返回查询结果
 */
export default (sql: string, args?: Array<any> | number | string): Promise<any> => {
    return new Promise((resolve, reject) => {
        db.query(sql, args, (error, results, fields) => {
            if (error) return reject(error);
            resolve(results);
        });
    })
}