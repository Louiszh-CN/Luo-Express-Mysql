import mysql from 'mysql2'
import type { Pool } from 'mysql2'

// const db = mysql.createPool({
//     host: '127.0.0.1',
//     user: 'root',
//     database: 'my_system',
//     port: 3306,
//     charset: 'utf8mb4'
// })
const db: Pool = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: '12345',
    database: 'my_system',
    port: 3306,
    charset: 'utf8mb4',
    connectionLimit: 10,    //最大连接数
    queueLimit: 0,      //指定等待队列的最大长度，队列长度超过限制，之后的请求将返回错误。默认值为 0，即没有队列长度限制。
    enableKeepAlive: true,      //是否启用 TCP 连接的 keep-alive 机制
    keepAliveInitialDelay: 0    //指定在连接上最初发送 keep-alive 数据包的等待时间。默认值为 0，表示立即发送 keep-alive 数据包。
})
export default db