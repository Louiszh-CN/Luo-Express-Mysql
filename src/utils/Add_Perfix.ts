/**
* @description 返回完整的头像url地址
* @param {*} params Array | Object
* @param {string} target 需要给哪个字段添加前缀
* @param {string} prefix 前缀是什么
* @returns 返回修改后数据
*/
export const Add_Img_Prefix = (params: any[] | Record<string, any>, target: string, prefix: string): any[] | Record<string, any> => {

    if (typeof params !== 'object' && (params == null || !Array.isArray(params))) {
        throw new Error('params must be an array or object');
    }

    // 如果 params 是数组
    if (Array.isArray(params)) {
        // 遍历数组，修改每个对象元素的 target 属性
        return params.map((item) => {
            item[target] = `${prefix}/${item[target]}`;
            return item;
        });
    }
    // 如果 params 是对象
    else {
        // 修改对象的 target 属性
        params[target] = `${prefix}/${params[target]}`;
        return params
    }
}