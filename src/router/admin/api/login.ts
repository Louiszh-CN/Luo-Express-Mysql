import sqlQuery from "@/db/sqlQuery"
import { secretKey } from '@/config'
import jwtToken from 'jsonwebtoken'
import type { Request, Response } from 'express'



module.exports = async (req: Request, res: Response) => {
    let { username, password } = req.body
    let query = 'select id,alias,job,profile from account where username=? and password=?'
    //执行查询
    sqlQuery(query, [username, password]).then(async ([result]) => {
        if (!!result) {
            const token = jwtToken.sign(result, secretKey, { algorithm: 'HS256', expiresIn: '24h' }) // 86400s = 1天
            res.send({
                code: 200,
                data: {
                    token: token
                },
                msg: '登录成功'
            })
        } else {
            res.send({ code: 401, msg: '账号或密码错误' })
        }
    }).catch((error) => {
        res.send({
            code: 500,
            msg: '服务器错误,可能为数据库连接失败,或数据库密码不正确',
        })
    })
}

