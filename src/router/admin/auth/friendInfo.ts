import sqlQuery from '@/db/sqlQuery'
import { Add_Img_Prefix } from '@/utils/Add_Perfix'
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

module.exports = async (req: RequestToken, res: Response) => {
    const { friendId } = req.params
    const query = 'select id,gender,age,alias,job,city,profile,email from account where id=?'
    const result = await sqlQuery(query, [friendId])
    const friendInfo = Add_Img_Prefix(result[0], 'profile', '/users')
    res.send({
        code: 200,
        data: friendInfo,
        message: 'success'
    })
}