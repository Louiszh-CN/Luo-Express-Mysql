import sqlQuery from '@/db/sqlQuery'
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

module.exports = async (req: RequestToken, res: Response) => {
    const query = "select id,emoji from emojis;"
    const data = await sqlQuery(query)
    res.send({
        code: 200,
        data
    })
}