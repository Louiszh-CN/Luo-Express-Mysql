import sqlQuery from "@/db/sqlQuery"
import { Add_Img_Prefix } from '@/utils/Add_Perfix'
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

module.exports = (req: RequestToken, res: Response) => {

    const { pageSize, pageIndex } = req.query
    const start = Number(pageSize) * (Number(pageIndex) - 1)
    const end = start + Number(pageSize)

    const query = `
        SELECT 
            a.id,
            a.alias AS name,
            a.gender,
            a.age,
            a.job,
            a.city,
            a.profile,
            a.email,
            c.role 
        FROM
            account AS a 
            LEFT JOIN user_role AS b 
            ON a.id = b.user_id 
            LEFT JOIN role as c 
            on b.role_id = c.id 
        limit 0, 5 
        `

    sqlQuery(query, [start, end]).then(data => {
        res.send({
            code: 200,
            data: Add_Img_Prefix(data, 'profile', '/users'),
            total: data.length,
            msg: 'ok'
        })
    }, err => {
        res.send({
            code: 500,
            msg: '服务器错误'
        })
    })
}