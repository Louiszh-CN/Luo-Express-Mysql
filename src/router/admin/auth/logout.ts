import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

module.exports = async (req: RequestToken, res: Response) => {

    res.send({
        code: 200,
        msg: '退出成功'
    })
}