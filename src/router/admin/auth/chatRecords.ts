import sqlQuery from "@/db/sqlQuery"
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

//获取聊天记录的接口
module.exports = async (req: RequestToken, res: Response) => {
    //用户token解析的id
    const { id } = req.user
    //前端传过来的指定好友id,查询聊天记录
    const { targetId } = req.query
    //查询id,内容,时间
    const query = 'SELECT id,chatId as userId,targetId,content,time FROM chatting_records WHERE (chatId=? AND targetId=?) OR (chatId=? AND targetId=?) ORDER BY time ASC'
    let result = await sqlQuery(query, [id, targetId, targetId, id])
    const data = result.map((item: any) => {
        //格式化日期
        const time = new Date(item.time).toLocaleString('zh-CH')
        return {
            ...item,
            mine: id == item.userId,
            time: time
        }
    })
    res.send({
        code: 200,
        targetId: Number(targetId),
        data
    })
}