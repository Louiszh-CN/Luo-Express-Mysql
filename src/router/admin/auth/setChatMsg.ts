import sqlQuery from '@/db/sqlQuery'
import dayjs from 'dayjs'
import type { RowDataPacket } from 'mysql2'


/**
 * 发送消息
 * @param {String} userId 用户id
 * @param {String} friendId 好友id
 * @param {String} msg 消息内容
 */
export default (userId: number, friendId: number, msg: string): Promise<RowDataPacket> => {
    const date = dayjs().format('YYYY-MM-DD HH:mm:ss')
    const query = `insert into chatting_records(chatId,targetId,content,time) values(?,?,?,?)`
    return new Promise(async (resolve, reject) => {
        const result = await sqlQuery(query, [userId, friendId, msg, date])
        result.affectedRows == 1 ? resolve(result) : reject(false)
    })
}