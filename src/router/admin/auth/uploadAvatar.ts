import multer from 'multer'
import sqlQuery from '@/db/sqlQuery'
import fse from 'fs-extra'
import path from 'path'
import type { Response, NextFunction } from 'express'
import type { RequestToken } from '@/interface/global'


const upload = multer({
    // 设置存储引擎
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'public/users')        // 保存的路径，备注：需要自己创建
        },
        filename: function (req, file, cb) {
            const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
            cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
        }
    }),
    // 过滤不是图片的文件
    fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
            return cb(new Error('Only image files are allowed!'));
        }
        cb(null, true);
    },
    //限制文件
    limits: {
        fileSize: 1024 * 1024 * 3
    }
}).single('avatar')



module.exports = (req: RequestToken, res: Response, next: NextFunction) => {
    const { id } = req.user

    const query = "UPDATE account SET PROFILE = ? WHERE id = ?"
    const getProfile = "SELECT profile FROM account WHERE id = ?"

    upload(req, res, (err) => {
        // 处理错误
        if (err instanceof multer.MulterError) {
            // 文件大小限制
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.send({ code: 500, msg: 'File upload error: 文件体积不能大于3MB' })
            }
            return res.send({ code: 500, msg: '未知错误' })
        }
        // 没有文件上传
        if (!req.file) return res.status(400).send('No file uploaded');
        const { filename } = req.file
        //删除原来的图片
        sqlQuery(getProfile, [id]).then(data => {
            const { profile } = data[0]
            if (profile != "defaultProfile.png") {
                fse.remove(`public/users/${profile}`)
            }
        })
        //修改数据库
        sqlQuery(query, [filename, id])

        res.send({
            code: 200,
            avatar: filename
        })
    })
}