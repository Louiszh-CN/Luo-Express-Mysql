import sqlQuery from "@/db/sqlQuery"
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'
import type { RowDataPacket } from 'mysql2'

module.exports = async (req: RequestToken, res: Response) => {
    const { id, alias, job, profile } = req.user
    const query = "select alias, job, profile from account where id = ?"

    sqlQuery(query, [id]).then((data: RowDataPacket[]) => {
        res.send({
            code: 200,
            data: {
                id: id,
                name: data[0].alias,
                profile: `/users/${data[0].profile}`,
                job: data[0].job,
            },
            msg: 'success',
        })
    })
}
