import sqlQuery from "@/db/sqlQuery"
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

module.exports = async (req: RequestToken, res: Response) => {
    const { id } = req.user

    //查询用户角色
    let roleQuery = 'SELECT role_id FROM user_role WHERE user_id = ?'
    //查询用户角色拥有的权限页面
    let authQuery = 'SELECT Permission FROM auth WHERE id IN (SELECT auth_id FROM role_auth WHERE role_id =?)'
    const [roleResult] = await sqlQuery(roleQuery, [id])
    const authResult = await sqlQuery(authQuery, [roleResult.role_id])

    res.send({
        code: 200,
        data: authResult.map((item: any) => item.Permission),
        mes: 'success'
    })
}