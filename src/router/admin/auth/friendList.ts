import sqlQuery from "@/db/sqlQuery"
import { Add_Img_Prefix } from '@/utils/Add_Perfix'
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

//查询好友列表
const query1 = `SELECT a2.id,a2.alias as name,a2.profile 
FROM 
    account a1 
JOIN friend_list f1 
    ON a1.id = f1.userId 
JOIN account a2 
    ON a2.id = f1.friendId 
where f1.userId = ?`
//查询好友列表
const query2 = `SELECT a1.id,a1.alias AS name,a1.profile
FROM 
    account a1 
JOIN friend_list f1 
    ON a1.id = f1.userId 
JOIN account a2 
    ON a2.id = f1.friendId
WHERE f1.friendId = ?`
//查询最后一条聊天记录
const query3 = "SELECT content,time FROM chatting_records WHERE chatId=? and targetId=? ORDER BY time DESC LIMIT 0,1"

//获取用户id的所有好友列表
module.exports = async (req: RequestToken, res: Response) => {
    const { id } = req.user
    const sqlResult1 = await sqlQuery(query1, [id])
    const sqlResult2 = await sqlQuery(query2, [id])
    //添加url前缀
    const addUrlData1 = Add_Img_Prefix(sqlResult1, 'profile', '/users') as any[]
    const addUrlData2 = Add_Img_Prefix(sqlResult2, 'profile', '/users') as any[]
    let data = [...addUrlData1, ...addUrlData2]
    //返回所有异步函数,用于并发请求获取最后一条聊天记录
    const sqlPromisArr = data.map((item) => {
        return sqlQuery(query3, [id, item.id])
    })
    //并发请求，获取所有记录,给data添加最后一条聊天记录
    Promise.all(sqlPromisArr).then((response) => {
        const result = data.map((item, index) => ({
            ...item,
            lastMsg: response[index][0]
        }))
        res.send({
            code: 200,
            data: result
        })
    }, (err) => {
        res.send({
            code: 4000,
            data: err
        })
    })
}