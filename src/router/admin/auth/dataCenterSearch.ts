import sqlQuery from '@/db/sqlQuery'
import { Add_Img_Prefix } from '@/utils/Add_Perfix'
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

//模糊查询
const query1 = `SELECT a2.id,a2.alias as name,a2.profile 
FROM 
    account a1 
JOIN friend_list f1 
    ON a1.id = f1.userId 
JOIN account a2 
    ON a2.id = f1.friendId 
where a2.alias like ?`
//模糊查询
const query2 = `SELECT a1.id,a1.alias AS name,a1.profile
FROM 
    account a1 
JOIN friend_list f1 
    ON a1.id = f1.userId 
JOIN account a2 
    ON a2.id = f1.friendId
WHERE a1.alias like ?`

module.exports = async (req: RequestToken, res: Response) => {
    //如果传递值为空则返回空数组
    if (!req.query.keyword) {
        return res.send({ code: 200, data: [] })
    }

    const keyword = `%${req.query.keyword}%`

    const result1 = await sqlQuery(query1, [keyword])
    const result2 = await sqlQuery(query2, [keyword])

    res.send({
        code: 200,
        data: Add_Img_Prefix([...result1, ...result2], 'profile', '/users')
    })
}
