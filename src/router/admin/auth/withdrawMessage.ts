import sqlQuery from "@/db/sqlQuery"
import type { RequestToken } from "@/interface/global"
import type { Response } from "express"


const query = "delete from chatting_records where chatId=? and id=? and targetId=?"

module.exports = async (req: RequestToken, res: Response) => {
    //用户id
    const { user } = req
    //消息id和好友id
    const { messageId, friendId } = req.body
    const result = await sqlQuery(query, [user.id, messageId, friendId])
    if (result.affectedRows >= 1) {
        res.send({
            code: 200,
            msg: '撤回成功'
        })
    } else {
        res.send({
            code: 40000,
            msg: '撤回失败'
        })
    }
}