import sqlQuery from '@/db/sqlQuery'
import dayjs from 'dayjs'
import type { Response } from 'express'
import type { RequestToken } from '@/interface/global'

module.exports = async (req: RequestToken, res: Response) => {
    const { pageSize, pageIndex } = req.query
    const start = Number(pageSize) * (Number(pageIndex) - 1)
    const end = start + Number(pageSize)
    const query = "SELECT id,book_name,author,introduction,time FROM books LIMIT ?,?"
    const query_total = "SELECT count(*) as total FROM books"

    try {
        let result = await sqlQuery(query, [start, end])
        const total_result = await sqlQuery(query_total)
        result = result.map((item: any) => ({ ...item, time: dayjs(item.time).format('YYYY-MM-DD HH:mm:ss') }))

        res.send({
            code: 200,
            data: result,
            total: total_result[0].total
        })

    } catch (e) {
        res.send({
            code: 500,
            msg: (<Error>e).message
        })
    }
}