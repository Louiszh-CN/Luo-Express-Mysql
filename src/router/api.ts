import express from "express"

const router = express.Router()

router.post('/login', require('./admin/api/login'))
//最新书籍
router.get('/book/newBooks', require('./readBook/api/newBooks.ts'))
//书籍详细信息
router.get('/book/detail', require('./readBook/api/bookDetail'))
//章节信息
router.post('/book/chapter', require('./readBook/api/bookChapterInfo'))
//目录信息
router.get('/book/chapterDirectory', require('./readBook/api/chapterDirectory'))
//搜索书籍
router.get('/book/search', require('./readBook/api/searchBooks'))


//获取音频
router.get('/music/songinfo', require('./music/api/getMusic'))


module.exports = router