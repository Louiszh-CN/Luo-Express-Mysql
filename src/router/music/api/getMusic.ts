import { Add_Img_Prefix } from '@/utils/Add_Perfix'
import sqlQuery from '@/db/sqlQuery'
import type { Request, Response } from 'express'



module.exports = async (req: Request, res: Response) => {
    const { musicid } = req.query
    //查询歌曲
    const songInfoQuery = `SELECT 
        id,
        author_id,
        name,
        lyric,
        cover,
        audio_name as play_url,
        TIME AS addTime
    FROM
        music_song 
    WHERE id = ? `

    //查询歌手
    const songAuthorQuery = `SELECT 
        id as authorId,
        name,
        time as addTime
    FROM
        music_author 
    WHERE id = ? `


    let [songInfo] = await sqlQuery(songInfoQuery, [musicid])
    const [author] = await sqlQuery(songAuthorQuery, [songInfo.author_id])

    //音频地址
    songInfo = Add_Img_Prefix(songInfo, 'play_url', '/music/audio')
    //封面地址
    songInfo = Add_Img_Prefix(songInfo, 'cover', '/music/img/cover')

    res.send({
        code: 200,
        data: { ...songInfo, author }
    })
}
