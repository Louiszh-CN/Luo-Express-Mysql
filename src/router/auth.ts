import express from "express"

const router = express.Router()
//登出
router.post('/logout', require('./admin/auth/logout'))
//获取登录用户信息
router.get('/userInfo', require('./admin/auth/userInfo'))
//好友列表
router.get('/friendList', require('./admin/auth/friendList'))
//获取聊天记录
router.get('/chatRecords', require('./admin/auth/chatRecords'))
//获取emojis图标
router.get('/emojis', require('./admin/auth/emojis'))
//撤回消息
router.post('/withdrawMessage', require('./admin/auth/withdrawMessage'))
//用户列表
router.get('/user/list', require('./admin/auth/userList'))
//好友详情
router.get('/friend/info/:friendId', require('./admin/auth/friendInfo'))
//消息搜索
router.get('/dataCenter/search', require('./admin/auth/dataCenterSearch'))
//获取菜单
router.get('/menu/list', require('./admin/auth/getMenuList'))
//获取所有图书信息
router.get('/library/list', require('./admin/auth/getLibrary'))
//上传头像
router.post('/upload/avatar', require('./admin/auth/uploadAvatar'))

module.exports = router
