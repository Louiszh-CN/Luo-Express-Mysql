import sqlQuery from "@/db/sqlQuery"
import type { Request, Response } from 'express'

module.exports = (req: Request, res: Response) => {
    const { id, chapterId } = req.body

    const sql = `SELECT 
        book_id as bookId,
        chapter_id as chapterId,
        chapter_name as name,
        chapter_content as content,
        content_added_time as time
    FROM books_chapters
        WHERE book_id = ? and chapter_id =?`

    sqlQuery(sql, [id, chapterId]).then((data: any) => {
        res.send({
            code: 200,
            data: data[0]
        })
    }).catch(err => {
        res.send({
            code: 500,
            msg: err
        })
    })
}