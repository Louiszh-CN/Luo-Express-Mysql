import sqlQuery from "@/db/sqlQuery"
import { Add_Img_Prefix } from "@/utils/Add_Perfix"
import type { Request, Response } from 'express'

module.exports = (req: Request, res: Response) => {
    const { id } = req.query

    const sql = "select id,book_name,author,introduction,img,time from books where id = ?"
    sqlQuery(sql, Number(id)).then((data: any) => {

        res.send({
            code: 200,
            data: Add_Img_Prefix(data[0], 'img', '/bookImg')
        })
    }, (err) => {
        res.send({
            code: 200,
            msg: err
        })
    })
}