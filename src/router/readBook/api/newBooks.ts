import sqlQuery from "@/db/sqlQuery"
import { Add_Img_Prefix } from "@/utils/Add_Perfix"
import type { Request, Response } from 'express'

module.exports = (req: Request, res: Response) => {
    const { start, end } = req.query
    const query = "select id,book_name,author,img,time from books order by time desc LIMIT ?,?"

    sqlQuery(query, [Math.min(Number(start) - 1, 0), Number(end)]).then((data: any) => {
        data = data.map((item: any) => Add_Img_Prefix(item, 'img', '/bookImg'))
        res.send({
            code: 200,
            data: data,
            total: data.length
        })
    }, (e) => {
        res.send({
            code: 200,
            msg: e
        })
    })
}