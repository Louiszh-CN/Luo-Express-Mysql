import sqlQuery from "@/db/sqlQuery"
import { Add_Img_Prefix } from "@/utils/Add_Perfix"
import type { Request, Response } from 'express'

module.exports = (req: Request, res: Response) => {
    const { search } = req.query
    if (!search) { return res.send({ code: 400, msg: '请输入搜索内容' }) }

    const sql = "select id,book_name as name,author,introduction,img from books where book_name like ? or author like ?"

    sqlQuery(sql, [`%${search}%`, `%${search}%`]).then((data: any) => {
        data = data.map((item: any) => Add_Img_Prefix(item, 'img', '/bookImg'))
        res.send({
            code: 200,
            msg: 'success',
            data
        })
    }, () => {
        res.send({
            code: 500,
            msg: 'err'
        })
    })
}