import sqlQuery from "@/db/sqlQuery"
import type { Request, Response } from 'express'

module.exports = (req: Request, res: Response) => {
    const { bookId } = req.query
    const query = "select chapter_id as chapterId,chapter_name as name from books_chapters where book_id = ?"

    sqlQuery(query, [bookId]).then((data: any) => {
        res.send({
            code: 200,
            data,
            total: data.length,
            msg: 'success',
        })
    }, () => {
        res.send({
            code: 500,
            msg: 'failed'
        })
    })
}