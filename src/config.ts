/**
 * @description token解析秘钥
 */
export const secretKey = 'LJZwwds ^_^'

/**
 * @description 服务端口号
 */
export const port = 3000

/**
 * @description false为http协议，true为https协议
 */
export const isHttps = false

/**
 * @description 请求的referer白名单
 */
export const whiteList = ['127.0.0.1', 'localhost', '120.79.75.51', '10.40.178.33']

