import type { Request } from 'express'

export interface Token {
    id: string
    alias: string
    job: string
    profile: string
}
//经过jwt解析后的请求信息
export interface RequestToken extends Request {
    user: Token
}
