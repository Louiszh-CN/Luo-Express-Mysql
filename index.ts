//引入该库，注册@为项目根目录 ==> 在package.json中配置
import 'module-alias/register'
import { secretKey, port, isHttps, whiteList } from '@/config'
import { expressjwt } from 'express-jwt'
import fs from 'fs'
import path from 'path'
import express from 'express'


const app = express()
//获取ssl证书和秘钥
const key = fs.readFileSync('./ssl.key', 'utf-8')
const cert = fs.readFileSync('./ssl.crt', 'utf-8')
const { createServer } = isHttps ? require('https') : require('http')
//http服务
const httpServer = createServer(isHttps ? { key: key, cert: cert } : void 0, app)

//socket服务
import { WebSocketServer } from 'ws'
const wss = new WebSocketServer({ server: httpServer })

import setChatMsg from './src/router/admin/auth/setChatMsg'

import type { WebSocket } from 'ws'
import type { Request, Response, NextFunction } from 'express'





//websocket连接数量
let wsConnectionsNumber = 0
//所有连接服务的用户
let clientPool = new Map()

wss.on('connection', (ws: WebSocket, req: Request) => {
    //获取url对象
    let url = new URL(req.url, `http://${req.headers.host}`)
    //url参数
    let params = url.searchParams
    //用户id唯一标识
    let userId = params.get('id')
    //不传递id,直接关闭连接
    !userId && ws.close()
    if (clientPool.get(userId)) {
        // console.log(`用户id=>${userId},已经连接过了`)
        return
    }
    //加入到用户池当中,用于后期发送信息
    clientPool.set(userId, ws)
    // console.log(`用户id=>${userId},socket连接成功,连接数量为${++wsConnectionsNumber}`)

    //socket报错处理
    ws.on('error', console.error)
    //接收客户端信息
    ws.on('message', async (data: Buffer, isBinary) => {
        const decoder = new TextDecoder('utf-8')
        //传递的消息格式不对,抛出错误
        try {
            //用户id（读取有符号的大端格式32位整数）
            const userId = data.readUInt32BE(0)
            //目标用户id
            const targetId = data.readUInt32BE(4)
            //消息长度
            const msgLength = data.readUInt32BE(8)
            //消息体
            const msg = decoder.decode(data.subarray(12, 12 + msgLength))
            //客户端实例
            const friend = clientPool.get(String(targetId))
            const client = clientPool.get(String(userId))
            const newData = Buffer.alloc(data.length)
            //复制data的前8个字节到newData的前8个字节中
            data.copy(newData, 0, 0, 8)
            //复制data第12个字节以后的数据到,newData中的第12个字节以后
            data.copy(newData, 12, 12)
            //如果查询到用户且在线则发送数据
            if (targetId && friend) {
                //数据库插入发送的信息
                const ok = await setChatMsg(userId, targetId, msg)
                //数据库插入成功后的id，放入到buffer中的第8个字节中
                newData.writeUInt32BE(ok.insertId, 8)
                if (ok.affectedRows >= 1) {
                    friend.send(data, { binary: isBinary })
                    client.send(data)
                }
            } else if (targetId) { //如果用户不在线则直接保存到数据库
                const ok = await setChatMsg(userId, targetId, msg)
                //数据库插入成功后的id，放入到buffer中的第8个字节中
                newData.writeUInt32BE(ok.insertId, 8)
                if (ok.affectedRows >= 1) {
                    client.send(newData)
                } else {
                    client.send(newData)
                }
            }
        } catch (error) {
            console.log(error)
        }
    })
    ws.on('close', () => {
        // console.log(`客户端用户=>${userId},退出连接`)
        //断开连接删除用户池中的用户
        clientPool.delete(userId)
        wsConnectionsNumber--
    })
})


//防盗链
app.use((req: Request, res: Response, next: NextFunction) => {
    const referer = req.get('referer')
    if (referer) {
        const { hostname } = new URL(referer)
        if (!whiteList.includes(hostname)) {
            return res.status(403).send('无效的来源')
        }
    }
    next()
})


app.use(express.static(path.resolve('public')))
//用于解析url-encoded格式的请求体数据
app.use(express.urlencoded({ extended: true }))
//用于解析json格式的请求体数据
app.use(express.json())
//解析jwtToken
app.use('/auth', expressjwt({ secret: secretKey, algorithms: ['HS256'], requestProperty: 'user' }))


//不需要token验证
app.use('/api', require('@/router/api'))
//需要通过token验证
app.use('/auth', require('@/router/auth'))
app.use('/robots.txt', (req: Request, res: Response) => {
    fs.readFile("./robots.txt", "utf-8", (err: NodeJS.ErrnoException | null, data: Buffer | string) => {
        if (err) return res.status(500).end('not find')
        res.status(200).end(data)
    })
})

//错误中间件
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    //拦截token错误
    if (err.name == 'UnauthorizedError') {
        return res.send({
            code: 40001,
            message: '无效token'
        })
    }
    res.status(500).send(err.messsage)
    //该路由上所有剩余的路由回调被绕过
    next('router')
})


httpServer.listen(port, () => {
    console.log(`${isHttps ? "https" : "http"}协议${port}端口已经开启`)
})
httpServer.on('error', (err: any) => {
    console.log(err)
})
httpServer.on('close', () => {
    console.log('服务已关闭')
})

